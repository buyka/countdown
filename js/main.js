// Count days hours minutes and seconds
 function countDown() {   
    const newYearDay ='1 Jan 2022';
    const dateNow = new Date();
    const dateEnd = new Date(newYearDay);
    const seconds = (dateEnd - dateNow)/1000;
    const days = Math.floor(seconds/24/3600);
    const hours = Math.floor(seconds/3600) % 24;
    const minutes = Math.floor(seconds/60) % 60 % 24;
    const secs = Math.floor( seconds % 60 );
    const dayDiv = document.querySelector(".day");
    const hourDiv = document.querySelector(".hour");
    const minuteDiv = document.querySelector(".minute");
    const secondDiv = document.querySelector(".second");
    dayDiv.textContent = days;
    hourDiv.textContent = ("0" + hours).slice(-2);
    minuteDiv.textContent = ("0" + minutes).slice(-2);
    secondDiv.textContent = ("0" + secs).slice(-2);
}

// Reload page every 1000 milliseconds 
// (OR every seconds).
window.setInterval(countDown, 1000); 	
// window.setInterval(function(){console.log("setInterval ajilj baina")}, 1000); 	

// Add audio player
let element= document.querySelector(".audio-player");
setTimeout(function(){
    element.classList.add("audio-player--display");
}, 10000);
// element.mouseenter(function () {
    //     element.classList.remove("audio-player--display");
    //     console.log(element.classList.contains("audio-player--display"))
    // });
    
    // Random Images
    function randomBackground() {let rndimg = ["images/pexels-cottonbro-3171770.jpg", "images/pexels-peter-spencer-1317365.jpg", "images/pexels-tejas-prajapati-721247.jpg"]; 
    let x=(Math.floor(Math.random()*rndimg.length));
    let randomimage=(rndimg[x]);
    document.querySelector("body").style.backgroundImage = "url("+ randomimage +")"; 
}

// Reload page every 1000 milliseconds 
// (OR every seconds).
// window.setInterval(randomBackground, 5000);	
